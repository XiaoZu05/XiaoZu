package com.bwie.user.service;

import com.bwie.common.domain.User;
import com.bwie.common.domain.response.VoEmitUser;
import com.bwie.common.result.Result;

import java.util.List;

public interface UserService {
    User findPhone(String phone);


    String add(User user);

    String upd(User user);

    String del(Integer userId);

    List<User> userList(VoEmitUser voEmitUser);

    User findMail(String userMail);
}

