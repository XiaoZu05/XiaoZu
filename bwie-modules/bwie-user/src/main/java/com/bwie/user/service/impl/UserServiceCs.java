package com.bwie.user.service.impl;

import com.bwie.common.domain.User;
import com.bwie.common.domain.response.VoEmitUser;
import com.bwie.common.result.Result;
import com.bwie.user.mapper.UserMapper;
import com.bwie.user.service.UserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceCs implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User findPhone(String phone) {
        return userMapper.findPhone(phone);
    }
    @Override
    public User findMail(String userMail) {
        return userMapper.findMail(userMail);
    }
    @Override
    public String add(User user) {
        Integer add = userMapper.add(user);
        if (add>0){
            return "添加成功";
        }
        return "添加失败";
    }

    @Override
    public String upd(User user) {
        Integer upd = userMapper.upd(user);
        if (upd>0){
            return "添加成功";
        }
        return "添加失败";
    }

    @Override
    public String del(Integer userId) {
        Integer del = userMapper.del(userId);
        if (del>0){
            return "添加成功";
        }
        return "添加失败";
    }

    @Override
    public List<User> userList(VoEmitUser voEmitUser) {
        PageHelper.startPage(voEmitUser.getPageNum(),voEmitUser.getPageSize());
        List<User> users = userMapper.userList(voEmitUser);
        return users;
    }


}
