package com.bwie.user.mapper;

import com.bwie.common.domain.User;
import com.bwie.common.domain.response.VoEmitUser;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    User findPhone(String phone);

    Integer add(User user);

    Integer upd(User user);

    Integer del(Integer userId);

    List<User> userList(VoEmitUser voEmitUser);

    User findMail(String userMail);
}
