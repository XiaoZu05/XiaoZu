package com.bwie.user.controller;

import com.bwie.common.domain.User;
import com.bwie.common.domain.response.VoEmitUser;
import com.bwie.common.result.Result;
import com.bwie.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/findPhone/{phone}")
    public Result findPhone(@PathVariable String phone){
        User phone1 = userService.findPhone(phone);
        return Result.success(phone1);
    }
    @PostMapping("findMail/{userMail}")
    public Result findMail(@PathVariable String userMail){
        User mail = userService.findMail(userMail);
        return  Result.success(mail);
    }
    @PostMapping("add")
    public Result add(@RequestBody User user){
       return Result.success(user);
    }
    @PostMapping("upd")
    public Result upd(@RequestBody User user){
       return Result.success(user);
    }
    @PostMapping("del/{userId}")
    public Result del(@PathVariable Integer userId){
        return Result.success(userId);
    }
    @PostMapping("userList")
    public Result userList(@RequestBody VoEmitUser voEmitUser){
        return Result.success(voEmitUser);
    }
}
