//package com.bwie.gateway.filters;
//
//import com.bwie.common.constants.TokenConstants;
//import com.bwie.common.utils.JwtUtils;
//import com.bwie.common.utils.StringUtils;
//import com.bwie.gateway.config.IgnoreWhiteConfig;
//import com.bwie.gateway.utils.GatewayUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.gateway.filter.GatewayFilterChain;
//import org.springframework.cloud.gateway.filter.GlobalFilter;
//import org.springframework.core.Ordered;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.http.server.reactive.ServerHttpRequest;
//import org.springframework.stereotype.Component;
//import org.springframework.web.server.ServerWebExchange;
//import reactor.core.publisher.Mono;
//
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
//@Component
//public class Filters implements GlobalFilter, Ordered {
//    @Autowired
//    private RedisTemplate<String,String> redisTemplate;
//
//    @Autowired
//    private IgnoreWhiteConfig ignoreWhiteConfig;
//    @Override
//    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        ServerHttpRequest request = exchange.getRequest();
//        String path = request.getURI().getPath();
//        List<String> whites = ignoreWhiteConfig.getWhites();
//        if (StringUtils.matches(path,whites)){
//            return chain.filter(exchange);
//        }
//
//        String token = request.getHeaders().getFirst("token");
//        if (token==null){
//            return GatewayUtils.errorResponse(exchange,"token不能为空");
//        }
//
//        try {
//            JwtUtils.parseToken(token);
//        } catch (Exception e) {
//            return GatewayUtils.errorResponse(exchange,"token不正确");
//        }
//
//        String userKey = JwtUtils.getUserKey(token);
//        if (!redisTemplate.hasKey(TokenConstants.LOGIN_TOKEN_KEY+userKey)){
//            return GatewayUtils.errorResponse(exchange,"token过期");
//        }
//        redisTemplate.expire(TokenConstants.LOGIN_TOKEN_KEY+userKey,30, TimeUnit.MINUTES);
//        return chain.filter(exchange);
//    }
//
//    @Override
//    public int getOrder() {
//        return 0;
//    }
//}
