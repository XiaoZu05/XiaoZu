package com.bwie.controller;

import com.alibaba.fastjson.JSON;
import com.bwie.common.constants.TokenConstants;
import com.bwie.common.domain.User;
import com.bwie.common.domain.request.EmailEequest;
import com.bwie.common.domain.request.RequestUser;
import com.bwie.common.domain.response.JwtResponse;
import com.bwie.common.result.Result;
import com.bwie.common.utils.JwtUtils;
import com.bwie.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.security.auth.message.AuthException;
import javax.servlet.http.HttpServletRequest;

@RestController
public class AuthController {
    @Autowired
    private AuthService authService;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;


    @Autowired
    private HttpServletRequest request;
    @PostMapping("/sendCode/{phone}")
    public Result sendCode(@PathVariable String phone){
        return authService.sendCode(phone);
    }

    @PostMapping("/login")
    public Result login(@RequestBody RequestUser requestUser){
        return authService.login(requestUser);
    }

    @PostMapping("/getInfo")
    public Result<User> getInfo(){
        String token = request.getHeader("token");
        String userKey = JwtUtils.getUserKey(token);
        String s = redisTemplate.opsForValue().get(TokenConstants.LOGIN_TOKEN_KEY + userKey);
        User user = JSON.parseObject(s, User.class);
        return Result.success(user);
    }
    @PostMapping("findMail/{userMail}")
     public String findMail(@PathVariable String userMail) throws AuthException {
        return authService.findMail(userMail);
    }
    @PostMapping("/emailLogin")
    public JwtResponse emailLogin(@RequestBody EmailEequest emailEequest){
        return  authService.emailLogin(emailEequest);
    }
}
