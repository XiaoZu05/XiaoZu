package com.bwie.service;

import com.bwie.common.domain.request.EmailEequest;
import com.bwie.common.domain.request.RequestUser;
import com.bwie.common.domain.response.JwtResponse;
import com.bwie.common.result.Result;

import javax.security.auth.message.AuthException;

public interface AuthService {
    Result sendCode(String phone);

    Result login(RequestUser requestUser);

    JwtResponse emailLogin(EmailEequest emailEequest);

    String findMail(String userMail);
}
