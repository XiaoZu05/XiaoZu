package com.bwie.service.impl;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSON;
import com.bwie.common.constants.JwtConstants;
import com.bwie.common.constants.TokenConstants;
import com.bwie.common.domain.User;
import com.bwie.common.domain.request.EmailEequest;
import com.bwie.common.domain.request.RequestUser;
import com.bwie.common.domain.response.JwtResponse;
import com.bwie.common.result.Result;
import com.bwie.common.utils.EmailUtil;
import com.bwie.common.utils.JwtUtils;
import com.bwie.common.utils.MsgUtil;
import com.bwie.common.utils.StringUtils;
import com.bwie.feign.Feign;
import com.bwie.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class AuthServiceCs implements AuthService {
    @Autowired
    private Feign feign;

    @Autowired
    private EmailUtil emailUtil;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private MsgUtil msgUtil;
    @Override
    public Result sendCode(String phone) {
        String code = RandomUtil.randomNumbers(4);
        String s = msgUtil.sendMsg(phone, code);
        if (!s.contains("成功")){
            msgUtil.sendMsg(phone,code);
        }
        redisTemplate.opsForValue().set("code",code);
        return Result.success(code,"发送成功");
    }

    @Override
    public Result login(RequestUser requestUser) {
        if (StringUtils.isMatch(requestUser.getPhone(),requestUser.getCode())){
            return Result.error("请先获取手机号");
        }
        Result<User> result = feign.findPhone(requestUser.getPhone());
        User data = result.getData();
        if (data==null){
            return Result.error("手机号不能为空");
        }
        HashMap<String, Object> map = new HashMap<>();
        String userKey = UUID.randomUUID().toString().replace("-", "");
        map.put(JwtConstants.USER_KEY,userKey);
        String token = JwtUtils.createToken(map);
        redisTemplate.opsForValue().set(TokenConstants.LOGIN_TOKEN_KEY+userKey, JSON.toJSONString(data),30, TimeUnit.MINUTES);
        JwtResponse jwtResponse = new JwtResponse();
        jwtResponse.setToken(token);
        jwtResponse.setExistTime("30MIn");
        return Result.success(jwtResponse,"登录成功");
    }

    @Override
    public String findMail(String userMail)  {
        User user = feign.findMail(userMail);
         if (user==null){
             throw new RuntimeException("邮箱号码不存在");
         }
        String code = RandomUtil.randomNumbers(4);
         emailUtil.sendCodeByEmail(code,userMail);
         redisTemplate.opsForValue().set("code",code);
         return code;
    }

    @Override
    public JwtResponse emailLogin(EmailEequest emailEequest) {
        User user = feign.findMail(emailEequest.getUserMail());
        if (user==null){
            throw new RuntimeException("邮箱号码不存在");
        }
        if (!redisTemplate.hasKey("code")){
            throw new RuntimeException("邮箱验证码过期");
        }

        String code = redisTemplate.opsForValue().get("code");
        if (!code.equals(emailEequest.getCode())){
            throw new RuntimeException("邮箱验证码错误");
        }

        HashMap<String, Object> map = new HashMap<>();
        String key = UUID.randomUUID().toString().replace("-", "");
        map.put(JwtConstants.USER_KEY,key);
        String token = JwtUtils.createToken(map);
        redisTemplate.opsForValue().set(TokenConstants.LOGIN_TOKEN_KEY,JSON.toJSONString(user),10,TimeUnit.MINUTES);

        JwtResponse jwtResponse = new JwtResponse();
        jwtResponse.setToken(token);
        jwtResponse.setExistTime("10MIN");
        return jwtResponse;
    }
}
