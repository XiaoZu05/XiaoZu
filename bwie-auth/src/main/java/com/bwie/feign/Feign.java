package com.bwie.feign;

import com.bwie.common.domain.User;
import com.bwie.common.result.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
@Component
@FeignClient("bwie-user")
public interface Feign {
    @PostMapping("/findPhone/{phone}")
    Result<User> findPhone(@PathVariable String phone);

    @PostMapping("findMail/{userMail}")
    public User findMail(@PathVariable String userMail);
}
