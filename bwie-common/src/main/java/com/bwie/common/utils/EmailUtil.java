package com.bwie.common.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

// jobkvhspqurtbgbg
@Component
public class EmailUtil {

    @Autowired
    private JavaMailSender javaMailSender;

    @CachePut(value = "aaa",key = "#email")
    public String saveCode(String code,String email){
        return code;
    }

    @Cacheable(value = "aaa",key = "#email")
    public String getCode(String email){
        return null;
    }

    //邮件发送 ===》封装的验证码发送方法
    public void sendCodeByEmail(String code,String email){

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            helper.setSubject("验证码通知");
            //helper.setText("<a href='http://10.1.75.101:8080/student/confirmRead?id="+student.getId()+"'>点击</a>",true);
            helper.setText("您的验证码是:"+code);
            helper.setFrom("3053669112@qq.com");
            helper.setTo(email);
            //helper.setTo(email);

            javaMailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

//    图片
//    @Test
//    public void test(){
//
//        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
//        try {
//            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
//            //标题
//            helper.setSubject("通知");
//            //内容
//            helper.setText("你好");
//            //发件人
//            helper.setFrom("2730874693@qq.com");
//            //收件人
//            helper.setTo("2730874693@qq.com");
//            //文件
//            helper.addAttachment("阿里云.txt",new File("C:\Users\1\Desktop"));
//
//            javaMailSender.send(mimeMessage);
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }
//    }


//		文件
//    @Test
//    public void test1(){
//
//        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
//        try {
//            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
//            //标题
//            helper.setSubject("通知");
//            //内容
//            helper.setText("你好,给你看张图片<br>" + "<img src='cid:opp'>",true);
//            //发件人
//            helper.setFrom("2736321299@qq.com");
//            //收件人
//            helper.setTo("2736321299@qq.com");
//            //图片
//            helper.addInline("opp",new FileSystemResource(new File("C:\Users\1\Desktop\213.jpg")));
//
//            javaMailSender.send(mimeMessage);
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }
//    }
}
