package com.bwie.common.domain;

import lombok.Data;

@Data
public class User {
    private Integer userId;
    private String userName;
    private String userPwd;
    private String phone;
    private String userBirthday;
    private String userCard;
    private Integer userSex;
    private String userMail;
    private Integer userState;
    private Integer roleId;
}
