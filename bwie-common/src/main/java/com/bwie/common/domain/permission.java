package com.bwie.common.domain;

import lombok.Data;

@Data
public class permission {
    private  Integer permissionId;
    private  String permissionName;
}
