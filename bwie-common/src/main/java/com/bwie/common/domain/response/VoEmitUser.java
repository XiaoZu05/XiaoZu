package com.bwie.common.domain.response;

import lombok.Data;

@Data
public class VoEmitUser {
    private String userName;
    private Integer pageNum=1;
    private Integer pageSize=5;
}
