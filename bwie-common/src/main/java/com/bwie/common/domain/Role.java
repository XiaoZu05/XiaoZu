package com.bwie.common.domain;

import lombok.Data;

@Data
public class Role {
    private Integer roleId;
    private String roleName;
}
