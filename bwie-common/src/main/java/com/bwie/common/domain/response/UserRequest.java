package com.bwie.common.domain.response;

import lombok.Data;

@Data
public class UserRequest {
    private String userPhone;
    private String code;
}
