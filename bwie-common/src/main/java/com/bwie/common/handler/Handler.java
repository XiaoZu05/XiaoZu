package com.bwie.common.handler;

import com.bwie.common.exception.ArgumentIllegalException;
import com.bwie.common.result.Result;
import lombok.extern.log4j.Log4j2;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@RestControllerAdvice
@Log4j2
public class Handler {
    @ExceptionHandler(Exception.class)
    public Result authException(HttpServletRequest request,Exception ex){
        String requestURI = request.getRequestURI();
        log.error("请求URI:{},出现了异常,异常信息:{}",requestURI,ex.getMessage());
        return Result.error(ex.getMessage());
    }


    @ExceptionHandler(ArgumentIllegalException.class)
    public Result authException1(HttpServletRequest request, ArgumentIllegalException ex){
        String requestURI = request.getRequestURI();
        log.error("请求URI:{},出现了异常,异常信息:{}",requestURI,ex.getMessage());
        return Result.error(ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public Result authException2(HttpServletRequest request, MethodArgumentTypeMismatchException e){
        String requestURI = request.getRequestURI();
        log.error("请求URI:{},出现了异常,异常信息:{}",requestURI,e.getMessage());
        return Result.error(String.format("请求参数类型不匹配，参数[%s]要求类型为：'%s'，但输入值为：'%s'", e.getName(), e.getRequiredType().getName(), e.getValue()));
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Object handleException(MethodArgumentNotValidException e){
        log.error(e.getMessage(),e);
        return Result.error(e.getBindingResult().getFieldErrors().stream().map(FieldError::getDefaultMessage).collect(Collectors.joining(";")));
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public Object handleConstrainException(ConstraintViolationException e){
        log.error(e.getMessage(),e);
        return Result.error(e.getMessage());
    }



}
