package com.bwie.common.exception;

import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

public class Exception extends RuntimeException{
    private int code;
    private String message;

    public Exception(){}

    public Exception(String message){
        super(message);
    }
    public Exception(int code,String message){
        this.code=code;
        this.message=message;
    }
}
