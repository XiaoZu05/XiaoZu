package com.bwie.common.exception;

import lombok.Data;

@Data
public class ArgumentIllegalException extends RuntimeException{
    private String message;

    public ArgumentIllegalException(){}

    public ArgumentIllegalException(String message){
        super(message);
        this.message=message;
    }
}
